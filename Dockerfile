##stage 1
FROM node:latest as node
WORKDIR /app
COPY package.json package.json
RUN npm install --legacy-peer-deps
COPY . .
RUN npm run build -- --prod

##stage 2
FROM nginx:1.20.1
COPY --from=node /app/dist/dockertest /usr/share/nginx/html